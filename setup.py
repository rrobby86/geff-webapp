from setuptools import setup

setup(
    name="GeFF",
    version="0.1",
    packages=["geff"],
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        "Flask>=0.12",
        "psycopg2>=2.7",
        "sqlbuilder>=0.7.10"
    ]
)
