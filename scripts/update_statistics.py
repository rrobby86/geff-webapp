#!/usr/bin/env python3

import psycopg2

from geff import app

def set_stat(cur, tid, iea, model_type, max_thr):
    if max_thr is None:
        max_thr = 0.0
    cur.execute("""
        SELECT COUNT(*) FROM statistics
        WHERE taxonomy_id = %s
        AND use_iea = %s
        AND model_type = %s
    """, (tid, iea, model_type))
    count, = cur.fetchone()
    if count == 1:
        cur.execute("""
            UPDATE statistics
            SET min_threshold = %s
            WHERE taxonomy_id = %s
            AND use_iea = %s
            AND model_type = %s
        """, (max_thr, tid, iea, model_type))
    elif count == 0:
        cur.execute("SELECT MAX(id) FROM statistics")
        id = cur.fetchone()[0] + 1
        cur.execute("""
            INSERT INTO statistics(id, taxonomy_id, use_iea, model_type, min_threshold)
            VALUES (%s, %s, %s, %s, %s)
        """, (id, tid, iea, model_type, max_thr))
    else:
        raise Exception("unexpected count > 1 of stat rows")

dsn = app.config["DATA_SOURCE"]
with psycopg2.connect(dsn) as conn:
    cur = conn.cursor()
    cur.execute("""
        CREATE TABLE IF NOT EXISTS statistics (
            id integer NOT NULL,
            taxonomy_id integer NOT NULL,
            use_iea boolean NOT NULL,
            model_type smallint NOT NULL,
            min_threshold real NOT NULL,
            CONSTRAINT statistics_pkey PRIMARY KEY (id),
            CONSTRAINT fk_statistics_taxonomy FOREIGN KEY (taxonomy_id)
                REFERENCES taxonomy (id) MATCH SIMPLE
                ON UPDATE NO ACTION
                ON DELETE NO ACTION
        )
    """)
    cur.execute("SELECT id FROM taxonomy")
    tids = list(tid for tid, in cur)
    for tid in tids:
        print("taxonomy #{}...".format(tid), end="")
        for iea in [False, True]:
            pred_table = "predictioniea" if iea else "prediction"
            cur.execute("""
                SELECT MAX(score_s1 + score_s2 + score_s3 + score_s4 + score_s5) / 5
                FROM {}
                WHERE taxonomy_id = %s
            """.format(pred_table), (tid, ))
            max_thr, = cur.fetchone()
            set_stat(cur, tid, iea, 0, max_thr)
            cur.execute("""
                SELECT score_s1, score_s2, score_s3, score_s4, score_s5
                FROM {}
                WHERE taxonomy_id = %s
            """.format(pred_table), (tid, ))
            max_thrs = 5 * [0.0]
            for vals in cur:
                svals = sorted(vals)
                max_thrs = [max(*v) for v in zip(svals, max_thrs)]
            for i, thr in zip(range(5, 0, -1), max_thrs):
                set_stat(cur, tid, iea, i, thr)
        print(" done")
    conn.commit()
