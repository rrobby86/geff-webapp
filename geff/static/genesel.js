Vue.component("taxonomy-list", {
  props: ["taxonomies"],
  render(elem) {
    var comp = this;
    return elem("select", {
      on: {
        change(ev) {comp.$emit("selected-taxonomy", ev.target.value);}
      }
    }, this.taxonomies.map(function(t) {
      return elem("option", {attrs: {value: t.id}}, t.name);
    }));
  },
  mounted() {
    $(this.$el).find("option:selected").removeAttr("selected");
  }
});

Vue.component("gene-list-item", {
  props: ["gene"],
  render(elem) {
    var comp = this;
    return elem("tr", [
      elem("td", [
        elem("a",
          {attrs: {
            href: "https://www.ncbi.nlm.nih.gov/gene/" + this.gene.id,
            target: "_blank",
            class: "tooltip",
            "data-tooltip": "See page on Entrez Gene DB"
          }},
          this.gene.id
        )
      ]),
      elem("td", this.gene.name),
      elem("td", this.gene.symbol),
      elem("td", [elem("button", {
        attrs: {
          type: "button",
          class: "btn btn-sm tooltip tooltip-left",
          "data-tooltip": "Remove"
        },
        on: {
          click() { comp.$emit("removeGene"); }
        }
      }, [elem("i", {attrs: {class: "icon icon-cross"}}, "")])])
    ]);
  }
});

Vue.component("gene-list", {
  props: ["genes"],
  render(elem) {
    var comp = this;
    if (this.genes.length) {
      header = elem("tr", ["Gene ID", "Gene name", "Gene symbol", ""]
        .map(function(h) {return elem("th", [h]);}));
      return elem("div", [
        elem("table", {
          class: "table table-striped table-hover"
        }, [header].concat(this.genes.map(function(gene, gindex) {
          return elem("gene-list-item", {
            props: {gene: gene},
            on: {
              removeGene() {
                comp.genes.splice(gindex, 1);
              }
            }
          });
        }))),
        elem("button", {
          attrs: {
            type: "button",
            class: "btn btn-sm"
          },
          on: {
            click() { comp.genes.splice(0, comp.genes.length); }
          }
        }, [
          elem("i", {attrs: {class: "icon icon-cross"}}, ""), " Clear"
        ])
      ]);
    } else {
      return elem("div",
        "If no gene selected, annotations of all genes of selected taxonomy will be shown");
    }
  }
});

Vue.component("gene-finder", {
  props: ["tid"],
  template: '<div class="ui-widget"><input class="geff-genesel-input" placeholder="e.g. BRCA1"></div>',
  mounted() {
    var comp = this;
    $(this.$el).find("input").autocomplete({
      source: function(request, response) {
        if (comp.tid !== null) {
          $.getJSON("_genes", {
            t: comp.tid,
            q: request.term
          }, response);
        } else {
          response([]);
        }
      },
      select: function(event, ui) {
        comp.$emit("gene-selected", ui.item);
      }
    })
    .autocomplete("instance")._renderItem = function(ul, item) {
      return $("<li>")
        .append($("<span class='geff-gene-symb'>").append(item.symbol))
        .append($("<span class='geff-gene-name'>").append(item.name))
        .appendTo(ul);
    };
  }
});

Vue.component("gene-selector", {
  props: ["tid", "genes"],
  render(elem) {
    if (this.tid !== null) {
      var comp = this;
      return elem("div", [
        elem("div", "Enter gene symbols (one at a time) to limit returned annotations"),
        elem("gene-finder", {
          props: {tid: this.tid},
          on: {
            "gene-selected": function(gene) {
              comp.genes.push(gene);
            }
          }
        }),
        elem("gene-list", {props: {genes: this.genes}})
      ])
    }
  },
  watch: {
    tid() {
      this.genes.splice(0, this.genes.length);
    }
  }
});
