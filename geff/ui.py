from flask import Blueprint, Response, render_template, request, current_app, url_for, stream_with_context
from flask.json import jsonify
from werkzeug.urls import url_encode

from geff import db

bp = Blueprint("ui", __name__)

@bp.app_template_global()
def modify_query(**new_values):
    args = request.args.copy()
    for key, value in new_values.items():
        args[key] = value
    return "{}?{}".format(request.path, url_encode(args))

@bp.route("/")
def home():
    return render_template("home.html")

@bp.route("/browse")
def known_form():
    return render_template("known.html",
            taxonomies=db.all_taxonomies().dictlist())

@bp.route("/_genes")
def genes_data():
    taxonomy_id = request.args["t"]
    user_query = request.args["q"]
    genes = db.taxonomy_genes_matching(taxonomy_id, user_query,
            current_app.config["MAX_GENES_MATCHING"]).dictlist()
    return jsonify(genes)

def _known_data():
    taxonomy_id = request.args["t"]
    include_iea = request.args.get("i", False)
    gene_ids_str = request.args.get("g", "")
    gene_ids = [int(i) for i in gene_ids_str.split(",")] if gene_ids_str else []
    sort_key = request.args.get("s", "gsymbol")
    sort_desc = bool(int(request.args.get("d", 0)))
    return (db.annotations(taxonomy_id, include_iea, gene_ids, sort_key, sort_desc),
            sort_key, sort_desc,
            url_for("ui.known_csv", t=taxonomy_id, i=request.args.get("i"), g=gene_ids_str))

@bp.route("/annotations")
def known_results():
    page = int(request.args.get("p", 0))
    data, sort_key, sort_desc, csv_url = _known_data()
    results = data.page(current_app.config["RESULTS_PER_PAGE"], page)
    taxonomy_name = db.get_taxonomy_name(request.args["t"])
    return render_template("results.html", results=results, columns=[
            ('gid', 'Gene ID'),
            ('gname', 'Gene name'),
            ('gsymbol', 'Gene symbol'),
            ('tgoid', 'GO term ID'),
            ('tname', 'GO term name'),
            ('ttype', 'GO type'),
            ('tlevel', 'GO level'),
            ('evidence', 'Evidence'),
            ('qualifier', 'Qualifier')
        ], sort_key=sort_key, sort_desc=sort_desc, section="known",
        caption="Known annotations for <i>{}</i>".format(taxonomy_name),
        csv_url=csv_url, new_search_url=url_for("ui.known_form"))

@bp.route("/annotations.csv")
def known_csv():
    results = _known_data()[0].tuples()
    def generate():
        yield "Gene ID;Gene name;Gene symbol;GO term ID;GO term name;GO term type;GO level;Evidence;Qualifier\n"
        for row in results:
            yield ";".join('"' + str(v) + '"' for v in row) + "\n"
    return Response(stream_with_context(generate()), mimetype="text/csv")

@bp.route("/predict")
def predict_form():
    return render_template("predict.html",
            taxonomies=db.all_taxonomies().dictlist())

@bp.route("/_stats")
def predict_stats():
    taxonomy_id = request.args["t"]
    stats = db.predictions_stats(taxonomy_id).dicts()
    return jsonify([r["min_threshold"] for r in stats])

def _predict_data():
    taxonomy_id = request.args["t"]
    use_iea = request.args.get("i", False)
    ensemble = int(request.args["e"])
    threshold = float(request.args["l"])
    leaf_only = request.args.get("b", False)
    gene_ids_str = request.args.get("g", "")
    gene_ids = [int(i) for i in gene_ids_str.split(",")] if gene_ids_str else []
    sort_key = request.args.get("s", "score")
    sort_desc = bool(int(request.args.get("d", 1)))
    return (db.predictions(taxonomy_id, ensemble, use_iea, threshold, leaf_only,
            gene_ids, sort_key, sort_desc), sort_key, sort_desc,
            url_for("ui.predict_csv", t=taxonomy_id, i=request.args.get("i"),
            e=ensemble, l=threshold, b=request.args.get("b"), g=gene_ids_str))

@bp.route("/predictions")
def predict_results():
    page = int(request.args.get("p", 0))
    data, sort_key, sort_desc, csv_url = _predict_data()
    results = data.page(current_app.config["RESULTS_PER_PAGE"], page)
    taxonomy_name = db.get_taxonomy_name(request.args["t"])
    return render_template("results.html", results=results, columns=[
            ('gid', 'Gene ID'),
            ('gname', 'Gene name'),
            ('gsymbol', 'Gene symbol'),
            ('tgoid', 'GO term ID'),
            ('tname', 'GO term name'),
            ('ttype', 'GO type'),
            ('tlevel', 'GO level'),
            ('score', 'Likelihood')
        ], sort_key=sort_key, sort_desc=sort_desc, section="predict",
        caption="Predicted annotations for <i>{}</i>".format(taxonomy_name),
        csv_url=csv_url, new_search_url=url_for("ui.predict_form"))

@bp.route("/predictions.csv")
def predict_csv():
    results = _predict_data()[0].tuples()
    def generate():
        yield "Gene ID;Gene name;Gene symbol;GO term ID;GO term name;GO term type;GO level;Likelihood\n"
        for row in results:
            yield ";".join('"' + str(v) + '"' for v in row) + "\n"
    return Response(stream_with_context(generate()), mimetype="text/csv")

@bp.route("/help")
def help():
    return render_template("help.html")
