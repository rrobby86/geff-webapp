class Page:

    def __init__(self, items, per_page, page, total):
        self._items = items
        self._ipp = per_page
        self._pn = page
        self._ti = total

    @property
    def not_empty(self):
        return len(self._items) > 0

    @property
    def page_index(self):
        return self._pn

    @property
    def page_num(self):
        return self._pn + 1

    @property
    def page_items(self):
        return self._items

    @property
    def page_first(self):
        return self._ipp * self._pn + 1

    @property
    def page_last(self):
        return min(self._ipp * (self._pn + 1), self._ti)

    @property
    def total_items(self):
        return self._ti

    @property
    def total_pages(self):
        return (self._ti - 1) // self._ipp + 1

    @property
    def per_page(self):
        return self._ipp
