from flask import Flask

def create_app(test_config=None):
    app = Flask(__name__)
    from geff import ui, db, config
    app.config.from_object(config)
    app.config.from_envvar("GEFF_CONFIG", silent=True)
    app.register_blueprint(ui.bp)
    app.teardown_appcontext(db.close_db)
    if app.debug:
        @app.after_request
        def no_cache(resp):
            resp.headers["Pragma"] = "no-cache"
            resp.headers["Expires"] = "0"
            resp.headers["Cache-Control"] = "public, max-age=0"
            return resp
    return app

app = create_app()
