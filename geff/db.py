from functools import reduce
import operator
from flask import g, current_app
from sqlbuilder.smartsql import Q, T, F, Cast, compile

from geff.page import Page

def connect_db():
    import psycopg2
    from psycopg2.extras import DictCursor
    conn = psycopg2.connect(current_app.config["DATA_SOURCE"])
    return conn

def get_db():
    if "db" not in g:
        g.db = connect_db()
    return g.db

def close_db(error=None):
    if "db" in g:
        g.db.close()

def _cursor(sql_and_params, cursor=None):
    if cursor is None:
        cursor = get_db().cursor()
    if current_app.config["QUERY_LOGGING"]:
        current_app.logger.debug("SQL: %s", sql_and_params[0])
    cursor.execute(*sql_and_params)
    return cursor

def _rows_as_dicts(cur):
    for row in cur:
        yield {cur.description[i][0]: val for i, val in enumerate(row)}

class Query:

    def __init__(self, query, *columns):
        self._query = query
        self._columns = columns

    def tuples(self):
        yield from _cursor(self._query.select(*self._columns))

    def dicts(self):
        return _rows_as_dicts(_cursor(self._query.select(*self._columns)))

    def dictlist(self):
        return list(self.dicts())

    def page(self, ipp, page=0):
        cur = _cursor(self._query.count())
        count, = cur.fetchone()
        cur = _cursor(self._query[page*ipp: page*ipp+ipp].select(*self._columns), cur)
        items = list(_rows_as_dicts(cur))
        return Page(items, ipp, page, count)

def all_taxonomies():
    return Query(Q().tables(T.taxonomy).order_by(F.name), F.id, F.name)

def get_taxonomy_name(tid):
    cur = _cursor(Q().tables(T.taxonomy).where(F.id == tid).select(F.name))
    return cur.fetchone()[0]

def taxonomy_genes_matching(tid, part, limit):
    query = Q().tables(T.gene) \
               .where((F.taxonomy_id == tid) & (F.symbol.icontains(part))) \
               .order_by(F.symbol)[:limit]
    return Query(query, F.id, F.name, F.symbol)

def annotations(tid, incl_iea, gene_ids=[], sort_key="gsymbol", sort_desc=False):
    sort = F(sort_key)
    if sort_desc:
        sort = sort.desc()
    query = Q() \
            .tables(T.association
                    + T.gene.on(T.association.gene_id == T.gene.id)
                    + T.goterm.on(T.association.goterm_id == T.goterm.id)) \
            .where((T.association.taxonomy_id == tid) & (T.goterm.name != '')) \
            .order_by(sort, T.association.id)
    if not incl_iea:
        query = query.where(T.association.evidence != "IEA")
    if gene_ids:
        query = query.where(T.association.gene_id.in_(gene_ids))
    return Query(query, T.gene.id__gid, T.gene.name__gname, T.gene.symbol__gsymbol,
              T.goterm.goid__tgoid, T.goterm.name__tname, T.goterm.type__ttype,
              T.goterm.level__tlevel, T.association.evidence, T.association.qualifier)

def predictions_stats(tid):
    return Query(Q().tables(T.statistics)
                    .where(F.taxonomy_id == tid)
                    .order_by(F.model_type, F.use_iea),
                F.model_type, F.use_iea, F.min_threshold)

def predictions(tid, ensemble, use_iea, thr, leaf_only, gene_ids=[], sort_key="score", sort_desc=True):
    sort = F(sort_key)
    if sort_desc:
        sort = sort.desc()
    pred_table = T.predictioniea if use_iea else T.prediction
    score_cols = [pred_table["score_s{}".format(i)] for i in range(1, 6)]
    mean_score = (reduce(operator.add, score_cols)) / 5
    if ensemble:
        model_cond = reduce(operator.add,
                [Cast(col >= thr, "integer") for col in score_cols]) >= ensemble
    else:
        model_cond = mean_score >= thr
    query = Q() \
            .tables(pred_table
                    + T.gene.on(pred_table.gene_id == T.gene.id)
                    + T.goterm.on(pred_table.goterm_id == T.goterm.id)) \
            .where((pred_table.taxonomy_id == tid) & (T.goterm.name != '') & model_cond) \
            .order_by(sort, pred_table.id)
    if leaf_only:
        query = query.where(T.goterm.leaf)
    if gene_ids:
        query = query.where(pred_table.gene_id.in_(gene_ids))
    return Query(query, T.gene.id__gid, T.gene.name__gname, T.gene.symbol__gsymbol,
              T.goterm.goid__tgoid, T.goterm.name__tname, T.goterm.type__ttype,
              T.goterm.level__tlevel, mean_score.as_("score"))
